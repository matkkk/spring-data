package hello;

public interface CustomerFirstNameProjection
{
    String getFirstName();
}
